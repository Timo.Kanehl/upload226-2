

2A

Problems with the original file:
* Usernames and password are stored in a plaintext dictionary, making it hard to add new users if one wishes to do so, and also making it easy for a potentiall attacker if they should get access to the code of app.py. Seperating it from the code and adding it to a database will make it both more structured and secure. The passwords are also stored in plaintext, increasing the risk of abuse even more.
* The code is not refactored at all, and all is in one file. It is also not following any best practises known from oop and other paradigms. This makes it hard to expand on it and to maintian the codebase for further use.
* It is no use of prepeared statements, leaving the program open to easy SQL-injection attacks.
* We can find unused code in the file, both in hello.py and in App.py. In app.py this is especially the case for def nocoffee() and gotcoffee(). This are not inheritly malicious and i cant see a straight forward way to abuse them, but having any code that is not fulfilling a necessary function is a potential security risk.
* It is no checking of the an entered password at all, so if you know or guess a username, you get access to the messagefunction without knowing the password.
* All information is sent in plaintext, opening for potential man in the middle attacks. 
* There is no kind of verification if the person who says that they are Alice actually is Alice.
* The application is also vulnarable to CSRF attacks as there is only varification in log in through flaskforms, but not afterwards. 


2B

My implementation:
* I did unfortunatly not finish all parts that i would wish that i had been able to do. I will therefore detail what i did and where I see potential improvements. 
* I have implemented a sqlite database for persistantly saving username and passwords. The passwords are also hashed by using bcrypt, so that each hash has its own salt, protecting from dictionary attacks, but not bruteforceing, as the has is know (saved at the beginning of the hash).
* When a user is trying to log inn, their hash password is compared against the saved hash.
* Passwords need to have at least 8 characters and have one lower case character, one uppercase character and one number. They can also not be longer than 64 characters.
* I have implemented the possibility to create new users. -
* I have implemented a log out button, making it easy to log out when finished using the application.
* I have also used prepared statements to prevent sqlinjection.
* Removed the GET possibility for the send-function. Now it can only post.

What I have not had the time to implement:
* An improved messaging application in regards of users not beeing able to say that they are who they want and all messages are also available to all. 
* CSRF-protection (implementing it as a part of the header)
* Implementation of logging features.

How to log in?
* One can either use the pre-programmed users alice with the password password123 and bob with the password bananas. These profiles have been exempt from the implemented password rules. You can also make your own user clicking on new user and then choosing a username and password. Neither of the fields can be left blank. 


Questiions:

1:


Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?
* Answer:
* Who might attack the application? Attackers may attack the application if they want either the userdata or messages that are exchanged over the messaging function.
* What damage can they do?
* Confidentiality: Steal and publish the data
* Integrity: They can manipulate the data. Also, since anyone can say that they are anyone, they could impersonate another person
* Availability: One could attack the availability through denial of service or ddos attacks. There is also a risk of the use of ransomware, which could both make the data unavaiable for the owner, while also giving the attacker the posibility to use it. 
* Are there limits to what the attacker can do? This application is still quite prone for attacks, but there are at least some limits to sql-injection attacks due to prepared statements. There is also most often a limit what it is sensible to prepare for, as an attacker with the right ressoruces mostly will find a way to bypass security mesuares, and it will therefore become a trade of between how important the data is that one needs to protect and the cost of doing so. This is the reason why top secret documents mostly are stored on pcs without internet connection.

What are the main attack vectors for the application?
* Answer: SQL-injections is the primary attack vector, besides also denial of service attacks or man in the middle attacks because of the lack of HTTPS. CSRF attatcks are also a possibility.

What should we do (or what have you done) to protect against attacks?
* Answer:
* Use of prepared statements against sql-injection
* Implementation of CSRF-protection also for the messaging part
* HTTPS to avoid attacks with wire shark
* Implement logging so that every log in and actionis possible to be reviewed and also mitigate some of the damage if someone should get access from outside.

What is the access control model?
* Answer: A good implementation would have implementet different roles with different priveleges, so that every person i an organization (or other use cases) only has access to the data that they actually need. Optimally this application could have implemented a mix between role based acces (giving a user with admin privileges possibility to have some kind of controll over the use (moderation etc)) and relationbased access control to possibly giving the users access to edit their own messages for a short while.

How can you know that you security is good enough? (traceability)
* Answer: In this application it is unfortunatly not good enough. With more time i would have liked to implement logging functionality that would help identifying security breaches 