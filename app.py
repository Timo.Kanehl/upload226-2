from http import HTTPStatus
from secrets import token_hex

import bcrypt
import flask
from bcrypt import hashpw, gensalt
from flask import Flask, abort,   make_response
from werkzeug.datastructures import WWWAuthenticate

from base64 import b64decode
import sys
import apsw
from apsw import Error

from pygments.formatters import HtmlFormatter
from threading import local

tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = token_hex(24)

# Add a login manager to the app
import flask_login
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# csrf = CSRFProtect(app)


# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# # This method is called whenever the login manager needs to get
# # the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    if not existing_username(user_id):
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid, passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        if check_valid_user(uid, passwd):
            return user_loader(uid)
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate=WWWAuthenticate('Basic realm=inf226, Bearer'))

@app.login_manager.unauthorized_handler
def unauthorized():
    return flask.redirect('/login')


def add_to_users(username, password):
    stmt = f"INSERT INTO users (username, password) values (?, ?);"
    psw = hashpw(password.encode('utf-8'), gensalt()).decode("utf-8")
    conn.execute(stmt, (username, psw))


def check_valid_user(username, password):
    hashed = get_password(username)
    return bcrypt.checkpw(password.encode('utf-8'), hashed.encode('utf-8'))


def get_password(username):
    stmt = f"SELECT password FROM users WHERE username = ?;"
    c = conn.execute(stmt, (username,))
    row = c.fetchone()
    c.close()
    result = row[0]
    return result


def existing_username(username):
    stmt = f"SELECT * FROM users WHERE username = ?;"
    c = conn.execute(stmt, (username,))
    row = c.fetchone()
    c.close()
    return row is not None

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY,
        sender TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY,
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
            id integer PRIMARY KEY,
            username TEXT NOT NULL,
            password TEXT NOT NULL);''')

    if c.execute('SELECT COUNT(*) FROM users').fetchone()[0] == 0:
        c.execute(
            f'INSERT INTO users (username, password) VALUES ("alice", "{hashpw("password123".encode("utf-8"), gensalt()).decode("utf-8")}"), ("bob", "{hashpw("bananas".encode("utf-8"), gensalt()).decode("utf-8")}")')

except Error as e:
    print(e)
    sys.exit(1)


import app_files.messaging
import app_files.paths