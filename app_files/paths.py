import flask
from flask import send_from_directory, request, render_template
from flask_login import login_required, login_user, logout_user, current_user

from app import app, check_valid_user, user_loader, existing_username, add_to_users
from login_form import LoginForm, NewUserForm

def password_req(password):
    password = str(password)
    if len(password) < 8:
        return False
    if len(password) > 64:
        return False
    if not any(char.isupper() for char in password):
        return False
    if not any(char.islower() for char in password):
        return False
    if not any(char.isdigit() for char in password):
        return False
    return True


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                               'templates/index.html', mimetype='text/html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        if check_valid_user(username, password):
            user = user_loader(username)
            login_user(user)
            flask.flash('Logged in successfully.')

            return flask.redirect(flask.url_for('index_html'))
    return render_template('./login.html', form=form)

@app.get('/logout')
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('login'))

@app.route('/registernewuser', methods=['GET', 'POST'])
def registernewuser():
    # A user that is already logged in should not be at the registration page
    if current_user.is_authenticated:
        return flask.redirect(flask.url_for('index_html'))

    form = NewUserForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        if password_req(password) and len(username) > 0 and not existing_username(username):
            add_to_users(username, password)
            return flask.redirect(flask.url_for('index_html'))

    return render_template('./newUser.html', form=form)

